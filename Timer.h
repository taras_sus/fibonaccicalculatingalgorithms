#pragma once
#include <windows.h>
class Timer
{
public:
	Timer();
	void StartTimer();
	double StopTimer();
	~Timer();
private:
	LARGE_INTEGER start;
	LARGE_INTEGER stop;
	double frequency;
};
