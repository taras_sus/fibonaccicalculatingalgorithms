#pragma once

typedef unsigned long long ull;

class Fibonacci
{
public:
	Fibonacci(); // No need to create objects, all functions are static
	~Fibonacci();
	// Prototypes
	static ull recursiveFibo(ull number);
	static ull recursiveFiboWithCache(ull number);
	static ull linearFiboWithOnMemory(ull number);
	static ull linearFiboWithO1Memory(ull number);
};

