#include "Fibonacci.h"
#include <vector>

Fibonacci::Fibonacci()
{
	// Nothing to do
}


Fibonacci::~Fibonacci()
{
	// Nothing to do
}

ull Fibonacci::recursiveFibo(ull number)
{
	if (number == 0) return 0;
	if (number <= 2) return 1;
	return (recursiveFibo(number - 1) + recursiveFibo(number - 2));
}

ull Fibonacci::recursiveFiboWithCache(ull number)
{
	if (number == 0) return 0;
	static std::vector<ull> fiboCache = { 1, 1, 2 };

	if (number <= fiboCache.size()) return fiboCache[number - 1];
	fiboCache.push_back(recursiveFiboWithCache(number - 1) + recursiveFiboWithCache(number - 2));
	return fiboCache[number - 1];
}

ull Fibonacci::linearFiboWithOnMemory(ull number)
{
	std::vector<ull> fiboNumbers = { 0, 1, 1 };
	for (size_t i = 3; i <= number; ++i)
	{
		fiboNumbers.push_back(fiboNumbers[i - 1] + fiboNumbers[i - 2]);
	}
	return fiboNumbers.back();
}

ull Fibonacci::linearFiboWithO1Memory(ull number)
{
	if (number == 0) return 0;
	if (number <= 2) return 1;
	ull previous = { 1 };
	ull current = { 1 };
	ull next = { 0 };
	for (int i = 3; i <= number; ++i)
	{
		next = previous + current;
		previous = current;
		current = next;
	}
	return next;
}