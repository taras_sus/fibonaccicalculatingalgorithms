#include <iostream>
#include <vector>
#include "Timer.h"
#include "Fibonacci.h"

void callFiboAlgorithm(ull(*algo)(ull number), ull number);
void runTests(int times, ull number);


int main()
{
	runTests(100, 37);
	system("pause");
	return EXIT_SUCCESS;
}

void callFiboAlgorithm(ull(*algo)(ull number), ull number)
{
	Timer timer;
	timer.StartTimer();
	algo(number); // call function
	std::cout << timer.StopTimer() << " secs" << std::endl;
}

void runTests(int times, ull number)
{
	std::cout << "Testing calculating of the Fibonacci Number" << std::endl;
	for (size_t i = 0; i < times; ++i)
	{
		std::cout << "recursiveFibo(" << number << "): \t\t";
		callFiboAlgorithm(Fibonacci::recursiveFibo, number);
		std::cout << std::endl;
		std::cout << "recursiveFiboWithCache(" << number << "):\t";
		callFiboAlgorithm(Fibonacci::recursiveFiboWithCache, number);
		std::cout << std::endl;
		std::cout << "linearFiboWithOnMemory(" << number << "):\t";
		callFiboAlgorithm(Fibonacci::linearFiboWithOnMemory, number);
		std::cout << std::endl;
		std::cout << "linearFiboWithO1Memory(" << number << "):\t";
		callFiboAlgorithm(Fibonacci::linearFiboWithO1Memory, number);
		std::cout << std::endl;
	}
}
