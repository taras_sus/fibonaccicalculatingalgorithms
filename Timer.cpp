#include "Timer.h"



Timer::Timer()
{
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	frequency = (double)freq.QuadPart;
}

void Timer::StartTimer()
{
	QueryPerformanceCounter(&start);
}

double Timer::StopTimer()
{
	QueryPerformanceCounter(&stop);
	return ((stop.QuadPart - start.QuadPart) / frequency);
}


Timer::~Timer()
{
}
